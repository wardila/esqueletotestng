package PagesObjets;

import java.io.File;
import java.util.Properties;

import MapsObjets.MapsObjectDemo;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import urilidadesExcel.ReadExcelFile;


public class PageObjectDemo extends MapsObjectDemo
{

	
	public PageObjectDemo(AppiumDriver<MobileElement>driver) {
		super(driver);
		this.driver =(AppiumDriver<MobileElement>) driver;
		// TODO Auto-generated constructor stub
	}

	public void calculadora(ReadExcelFile leer, Properties propiedades, File rutaCarpeta, String nomTest) throws Exception ,InterruptedException
	{
		try 
		{
			
			
			//INCIO DE BUSQUEDA GOOGLE
			click (nomTest,btnGoogle,rutaCarpeta);
			//INGRESA URL PAGINA
			sendKey(nomTest,leer.getCellValue(propiedades.getProperty("FileInputStream"),"capability",1,3),btnGoogle,rutaCarpeta);
			tiempoEspera(2000);
            enter(rutaCarpeta, nomTest);
            //SCROLL EN LA PAGINA 
            scrollVertical(rutaCarpeta,532,1400,0,4);
            scrollVertical(rutaCarpeta,532,1400,0,4);
            scrollVertical(rutaCarpeta,532,1000,0,4);
            tiempoEspera(2000);
            //INGRESA A OPCION DE ALERTAS 
            click (nomTest,btnAlertasMV,rutaCarpeta);
            tiempoEspera(2000);
            scrollVertical(rutaCarpeta,532,540,1400,3);
            tiempoEspera(2000);
            scrollVertical(rutaCarpeta,532,540,1400,3);
            scrollVertical(rutaCarpeta,532,540,1400,3);
            tiempoEspera(2000);
            //BUSCA BOTON ALERTAS 
            click (nomTest,btnAlertas,rutaCarpeta);
            scrollVertical(rutaCarpeta,532,540,1400,3);
            tiempoEspera(5000);
            // ALERTA 1 ACEPTAR 
            click (nomTest,btnAlerta1,rutaCarpeta);
            click (nomTest,aceptar,rutaCarpeta);
            scrollVertical(rutaCarpeta,532,1000,0,4);
            //ALERTA 2, 5 SEGUNOS ACEPTAR 
            click (nomTest,btnAlerta2,rutaCarpeta);
            tiempoEspera(6000);
            click (nomTest,aceptar,rutaCarpeta);
            //ALERTA 3 ACEPTAR
            click (nomTest,btnAlerta3,rutaCarpeta);
            click (nomTest,aceptar,rutaCarpeta);
            //ALERT 3 CANCELAR 
            click (nomTest,btnAlerta3,rutaCarpeta);
            click (nomTest,cancelar,rutaCarpeta);
            //ALERTA 4 INGRESAR TEXTO 
            click (nomTest,btnTexto,rutaCarpeta);
            sendKey(nomTest,leer.getCellValue(propiedades.getProperty("FileInputStream"),"capability",1,4),texto,rutaCarpeta);
            click (nomTest,aceptar,rutaCarpeta);
            click (nomTest,btnGoogle,rutaCarpeta);
			//INGRESA URL PAGINA
			sendKey(nomTest,leer.getCellValue(propiedades.getProperty("FileInputStream"),"capability",1,3),btnGoogle,rutaCarpeta);
			tiempoEspera(2000);
            enter(rutaCarpeta, nomTest);
            scrollVertical(rutaCarpeta,532,1400,0,4);
            scrollVertical(rutaCarpeta,532,1400,0,4);
            scrollVertical(rutaCarpeta,532,1400,0,4);
            tiempoEspera(2000);
            //SELECCIONAR WIDGETS
            click (nomTest,btnWidgets,rutaCarpeta);
            tiempoEspera(2000);
            scrollVertical(rutaCarpeta,532,540,1400,3);
            //BOTON SELECTOR DE FECHAS
            click (nomTest,btnSelectordeFechas,rutaCarpeta);
            scrollVertical(rutaCarpeta,532,540,1400,3);
            tiempoEspera(5000);
            //FECHA 1 
            click (nomTest,Fecha1,rutaCarpeta);
            clear (Fecha1, nomTest, rutaCarpeta);
            sendKey(nomTest,leer.getCellValue(propiedades.getProperty("FileInputStream"),"capability",1,5),Fecha1,rutaCarpeta);
            enter(rutaCarpeta, nomTest);
            //FECHA 2 
            click (nomTest,Fecha2,rutaCarpeta);
            clear (Fecha2, nomTest, rutaCarpeta);
            sendKey(nomTest,leer.getCellValue(propiedades.getProperty("FileInputStream"),"capability",1,6),Fecha2,rutaCarpeta);
           
            
           
            
            
			 
		} 
		catch (Exception e)
        {
			System.out.println(e);
		}
		
		
	  }
	}

	
	

	
	

