package ClaseBase;


import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import urilidadesExcel.ReadExcelFile;

@SuppressWarnings("unused")
public class ClaseBase {
	

	protected AppiumDriver<MobileElement>driver;
    //CONSTRUCTOR DE CLASE BASE
	    public ClaseBase(WebDriver driver)
	    {
	    super();
		// TODO Auto-generated constructor stub.,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,8888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888
	    }
		
	   @SuppressWarnings("rawtypes")
     //METODO DE NAVEGADOR
	  
	    public static  AppiumDriver appiumdriverConection(ReadExcelFile leer,Properties propiedades,String cap,int fila, int columna)  throws Exception
	    {
	        
	    	//SETEAR LAS PROPIEDADES DEL EJECUTABLE DE CHROME
	        AppiumDriver _driver=null;
	        
	        try
	           {
	        	//CAPIBILITES DE MOBILE 
	        	DesiredCapabilities caps=new DesiredCapabilities();
	        	caps.setCapability("plataformName", propiedades.getProperty("plataformName"));
	        	caps.setCapability("deviceName", propiedades.getProperty("deviceName"));
	        	caps.setCapability("platformVersion", propiedades.getProperty("platformVersion"));
	        	caps.setCapability("appPackage", leer.getCellValue(propiedades.getProperty("FileInputStream"),""+cap+"", 1, 1));
	        	caps.setCapability("appActivity", leer.getCellValue(propiedades.getProperty("FileInputStream"),""+cap+"", 1, 2));
	        	caps.setCapability("noReset", propiedades.getProperty("noReset"));
	        	caps.setCapability("autoGrantPermissions", propiedades.getProperty("utoGrantPermissions"));
	        	//INSTANCIAR APPIUM DRIVER
	         
	        try
	            {
	        	
	        	printConsola("cargando capability de appium, favor esperar ....");
	        	_driver =new AndroidDriver(new URL("http:/127.0.0.1:4723/wd/hub"),caps );
	            }
	        catch (MalformedURLException e)
	            {
				printConsola(e.getMessage());
			    }
	        
	        return _driver;
	        
	         }
	        
	        catch (Exception e)
	            {
	        	printConsola(e.getMessage());
			    }
	        //SETEAR LAS PROPIEDADES DEL NAVEGADOR 
	             return _driver;
	      
	          }

	       
	
	
		private static void printConsola(String message) {
		// TODO Auto-generated method stub
		
	}

		//METODO CLIK
	    public void click( String nomTest, By locator,File rutaCarpeta) throws Exception
	    {
	        driver.findElement(locator).click();
	        tiempoEspera(2000);
	        captureScreen(nomTest, rutaCarpeta);
	        
	    }

	    
	    //METODO PARA REMPLAZAR
	    public By localizadorVariable(By locator, String valor)throws Exception
	    
	    {
	    	String jj =locator.toString().replace("{0}", valor);
	    	String kk =jj.replace("By.xpath:", "");
	    	By localizador=By.xpath(kk);
	    	return localizador;
	    }
	    
	    
	    public List<MobileElement> listaElementos(By locator, File rutaCarpeta) throws Exception
	    {
	      
	    	List<MobileElement> elemento=driver.findElements(locator);
	    	return elemento;
	        
	    }
	    

	 public void variosNumeros( String nomTest,String numeros, By locator, File rutaCarpeta) throws Exception
	 {
		 
		String[]num=new String[numeros.length()];
		for(int i=0;i<numeros.length();i++)
		{
			num[i]=String.valueOf(numeros.charAt(i));
			click(nomTest,localizadorVariable(locator,num[i]),rutaCarpeta);
		}
	 }
	 
	    
	
	    
     //METODO BORRAR
	    public void clear(By locator, String nomTest,File rutaCarpeta) throws Exception
	    {
	        //driver.findElement(locator).clear();
	    	driver.findElement(locator).sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
	        captureScreen(nomTest, rutaCarpeta);
	        tiempoEspera(2000);
	    }



	   //METODO ENVIAR TEXTO
	    public void sendKey(String nomTest,String inputText,By locator,File rutaCarpeta) throws Exception
	    {
	        driver.findElement(locator).sendKeys(inputText);
	        captureScreen(nomTest, rutaCarpeta);
	        tiempoEspera(2000);
	    }
	        
	    
	    //METODO ENTER SUBMIN
	    public void submit(By locator,String nomTest, File rutaCarpeta) throws Exception
	    {
	        driver.findElement(locator).submit();
	        captureScreen(nomTest, rutaCarpeta);
	        tiempoEspera(2000);
	    }
	    
	   
		public void enter(File rutaCarpeta,String nomTest)throws Exception
        {
            
            ((AndroidDriver<MobileElement>) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
            tiempoEspera(2000);
            captureScreen(nomTest, rutaCarpeta);
        }
	    
	    @SuppressWarnings("rawtypes")
		public void tocarPantalla(int x, int y)
	    {
	    	TouchAction touch=new TouchAction(driver);
	    	touch.press(PointOption.point(x,y)).release().perform();
	    	
	    }
	    
 
	    @SuppressWarnings("rawtypes")
		public void scrollVertical(File rutaCarpeta,int xint, int yint, int yfinal, int interaciones)
	    {
	    	TouchAction touch=new TouchAction(driver);
	    	touch.press(PointOption.point(xint,yint))
	    	.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
	    	.moveTo(PointOption.point(xint,yfinal))
	    	.release().perform();
	    }
	    
	    //METODO TIEMPO DE ESPERA
	    public void tiempoEspera(long tiempo) throws InterruptedException
	    {
	        Thread.sleep(tiempo);
	    }
	    
	    public String fechahora()
	    {
	    	//TOMAMOS FECHA DEL SISTEMA
	    	LocalDateTime fechaSistema = LocalDateTime.now();
	    	//DEFINIR FORMATO FECHA 
	    	DateTimeFormatter fecha = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
	    	//DAR FORMATO A LA FECHA DEL SISTEMA 
	    	String formatFecha =fecha.format(fechaSistema);
	    	return formatFecha;
	    	
	    }

	    public String HoraSistema()
	    {
	    	//TOMAMOS LA FECHA DEL SISTEMA 
	    	LocalTime horaSistema = LocalTime.now();
	    	//DEFINIR FORMATO FECHA 
	    	DateTimeFormatter fecha = DateTimeFormatter.ofPattern("HHmmss");
	    	//DARFORMATO A LA FECHA DEL SISTEMA 
	    	String hora = fecha.format(horaSistema);
	    	return hora;
	    	
	    }
	    
	    public void captureScreen(String nomTest,File rutaCarpeta) throws Exception
	    {
	    	String hora = HoraSistema();
	    	File scrFile =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    	FileUtils.copyFile(scrFile,new File(rutaCarpeta+"\\"+hora+""+nomTest+".png"));
	    	
	    }
	    
	    public File crearCarpeta(Properties propiedades, String nomTest)
	    {
	    	//ALMACENAMOS LA FECHA EN EL SISTEMA 
	    	String fecha =fechahora();
	    	//CREAMOS EL NOMBRE DE LA CARPETA 
	    	String nomCarpeta =nomTest+"_"+fecha;
	    	//OBTENEMOS LA RUTA DE ALOJAMIENTO DE SALIDA Y EL NOMBRE DEL TEST A EJECUTAR
	    	File directorio =new File("./output/" + nomCarpeta);
	    	return directorio; 
	    	
	    }
	    
	    
}