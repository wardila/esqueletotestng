package PageObjects;

import java.io.File;
import java.io.IOException;


import org.openqa.selenium.WebDriver;

import MapsObjects.MapObjectCrearCuenta;


public class PageObjectCrearCuenta extends MapObjectCrearCuenta

 {
	public PageObjectCrearCuenta(WebDriver driver)
	{
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void ComprarProducto( File rutaCarpeta,String correo) throws Exception
	{
	//CIERRE DE COOKIES
	 click(cookies, rutaCarpeta);
	 //CANCELAR UBICACION      
	 click(cancelarUbicacion, rutaCarpeta);
	 tiempoEspera(2000);
	 //SELECCIONAR PRIMER PRODUCTO 
	 click(btnUniversal, rutaCarpeta);
	 tiempoEspera(2000);
	// CLICK AL BOTON DE COMPRA
	 click(btncomprarAhora, rutaCarpeta);
	 click(crearCuenta, rutaCarpeta);
    // CHECK AL BOTON DE AUTORIZAR
    click(checkAutorizo, rutaCarpeta);
    //DAR CLICK A CONTINUAR
    click(Continuar, rutaCarpeta);
    //CLICK VALIDAR CORREO
    click(confirmarCorreo, rutaCarpeta);
    // INGRESAR CORREO
    clear(txtCorreo, rutaCarpeta);
    sendKey(txtCorreo ,correo, rutaCarpeta);
    tiempoEspera(2000);
	}
	
	public void CreartuCuenta( File rutaCarpeta,String correo) throws Exception, IOException
	{
		
		
		tiempoEspera(2000);
		click(creatuCuenta, rutaCarpeta);
		// CHECK AL BOTON DE AUTORIZAR
		click(checkAutorizo, rutaCarpeta);
		//DAR CLICK A CONTINUAR
		click(Continuar, rutaCarpeta);
		   // INGRESAR CORREO
		click(confirmarCorreo, rutaCarpeta);
		 clear(txtCorreo, rutaCarpeta);
		 sendKey(txtCorreo,correo, rutaCarpeta);
		 tiempoEspera(2000);
	}

	
		
	}

	


