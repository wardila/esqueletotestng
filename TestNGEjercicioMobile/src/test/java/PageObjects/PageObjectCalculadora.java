package PageObjects;

import java.io.File;
import java.util.Properties;

import MapsObjects.MapsObjectSuma;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;



public class PageObjectCalculadora extends MapsObjectSuma
{

	
	public PageObjectCalculadora(AppiumDriver<MobileElement>driver) {
		super(driver);
		this.driver =(AppiumDriver<MobileElement>) driver;
		// TODO Auto-generated constructor stub
	}

	public void calculadora( File rutaCarpeta, String nomTest) throws Exception ,InterruptedException
	{
		try 
		{
			
			
			variosNumeros(nomTest,leer.getCellValue(propiedades.getProperty("FileInputStream"),"datos",1,1),btnNumero,rutaCarpeta);
		    click (nomTest,localizadorVariable(btnOperaciones,leer.getCellValue(propiedades.getProperty("FileInputStream"),"datos",1,0)), rutaCarpeta);  
		    variosNumeros(nomTest,leer.getCellValue(propiedades.getProperty("FileInputStream"),"datos",1,2),btnNumero,rutaCarpeta);
		    click (nomTest,btnIgual,rutaCarpeta);
			tiempoEspera(2000);
			click (nomTest,btnLimpiar,rutaCarpeta);
			 
			  
			 
		} 
		catch (Exception e)
        {
			System.out.println(e);
		}
		
		
	  }
	}

	
	

	
	

