package ClaseBase;

import java.io.File;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import urilidadesExcel.ReadExcelFile;

public class ClaseBase {
	
	protected static WebDriver driver;
     //CONSTRUCTOR DE CLASE
	    public ClaseBase(WebDriver driver) {
		// TODO Auto-generated constructor stub
	}
		
      //METODO DE NAVEGADOR
	  
	    public static  WebDriver chomeDriverConnetion(ReadExcelFile leer, Properties propiedades)
	    {
	        
	    	//SETEAR LAS PROPIEDADES DEL EJECUTABLE DE CHROME
	        System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
	        //DECLARAR EL OBJETO DRIVER TIPO CHROMEDRIVER
	        driver = new ChromeDriver();
	        
	        //MAXIMIZAR VENTANA DE NAVEGADOR
	        driver.manage().window().maximize();
	        
	        //INGRESAR LA URL DE PAGINA DE ACCESO
	        
	        return driver;
	      
	    }

      //METODO CLIK
	    public void click(By locator,File rutaCarpeta) throws Exception
	    {
	        driver.findElement(locator).click();
	        tiempoEspera(2000);
	        captureScreen(rutaCarpeta);
	        
	    }

	    public List<WebElement> listaElementos(By locator, File rutaCarpeta) throws Exception
	    {
	      
	    	List<WebElement> elemento=driver.findElements(locator);
	    	return elemento;
	        
	    }
	    
	    public String fechaQA()
	    {
	    	//TOMAMOS FECHA DEL SISTEMA
	    	LocalDateTime fechaSistema = LocalDateTime.now();
	    	//DEFINIR FORMATO FECHA 
	    	DateTimeFormatter fecha = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	    	//DAR FORMATO A LA FECHA DEL SISTEMA 
	    	String formatFecha =fecha.format(fechaSistema);
	    	return formatFecha;
 
	    }
	    
	    public String fechaQA2()
	    {
	    	//TOMAMOS FECHA DEL SISTEMA
	    	LocalDateTime fechaSistema = LocalDateTime.now();
	    	//DEFINIR FORMATO FECHA 
	    	DateTimeFormatter fecha = DateTimeFormatter.ofPattern("MMMMM-dd-yyyy-HHmm");
	    	//DAR FORMATO A LA FECHA DEL SISTEMA 
	    	String formatFecha =fecha.format(fechaSistema);
	    	return formatFecha;
 
	    }
	    
	
	    
     //METODO BORRAR
	    public void clear(By locator, File rutaCarpeta) throws Exception
	    {
	        //driver.findElement(locator).clear();
	    	driver.findElement(locator).sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
	        captureScreen(rutaCarpeta);
	        tiempoEspera(2000);
	    }



	   //METODO ENVIAR TEXTO
	    public void sendKey(By locator,String inputText,File rutaCarpeta) throws Exception
	    {
	        driver.findElement(locator).sendKeys(inputText);
	        captureScreen(rutaCarpeta);
	        tiempoEspera(2000);
	    }
	        
	    
	    //METODO ENTER SUBMIN
	    public void submit(By locator, File rutaCarpeta) throws Exception
	    {
	        driver.findElement(locator).submit();
	        captureScreen(rutaCarpeta);
	        tiempoEspera(2000);
	    }
	    
 
	    
	    //METODO TIEMPO DE ESPERA
	    public void tiempoEspera(long tiempo) throws InterruptedException
	    {
	        Thread.sleep(tiempo);
	    }
	    
	    public String fechahora()
	    {
	    	//TOMAMOS FECHA DEL SISTEMA
	    	LocalDateTime fechaSistema = LocalDateTime.now();
	    	//DEFINIR FORMATO FECHA 
	    	DateTimeFormatter fecha = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
	    	//DAR FORMATO A LA FECHA DEL SISTEMA 
	    	String formatFecha =fecha.format(fechaSistema);
	    	return formatFecha;
	    	
	    }

	    public String HoraSistema()
	    {
	    	//TOMAMOS LA FECHA DEL SISTEMA 
	    	LocalTime horaSistema = LocalTime.now();
	    	//DEFINIR FORMATO FECHA 
	    	DateTimeFormatter fecha = DateTimeFormatter.ofPattern("HHmmss");
	    	//DARFORMATO A LA FECHA DEL SISTEMA 
	    	String hora = fecha.format(horaSistema);
	    	return hora;
	    	
	    }
	    
	    public void captureScreen(File rutaCarpeta) throws Exception
	    {
	    	String hora = HoraSistema();
	    	File scrFile =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    	FileUtils.copyFile(scrFile,new File(rutaCarpeta+"\\"+hora+".png"));
	    	
	    }
	    
	    public File crearCarpeta(Properties propiedades, String nomTest)
	    {
	    	//ALMACENAMOS LA FECHA EN EL SISTEMA 
	    	String fecha =fechahora();
	    	//CREAMOS EL NOMBRE DE LA CARPETA 
	    	String nomCarpeta =nomTest+"_"+fecha;
	    	//OBTENEMOS LA RUTA DE ALOJAMIENTO DE SALIDA Y EL NOMBRE DEL TEST A EJECUTAR
	    	File directorio =new File("./output/" + nomCarpeta);
	    	return directorio; 
	    	
	    }
	    
	    
}