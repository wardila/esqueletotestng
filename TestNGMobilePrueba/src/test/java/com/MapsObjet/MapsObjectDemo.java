package com.MapsObjet;

import org.openqa.selenium.By;

import com.ClaseBase.ClasesBase;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class MapsObjectDemo extends ClasesBase
{
	 public MapsObjectDemo(AppiumDriver<MobileElement>driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	 
	 
	
	 protected By btnGoogle =By.id("com.android.chrome:id/url_bar");
	 protected By btnAlertasMV =By.xpath("//android.widget.TextView[@text='Alertas, marco y ventanas']");
	 protected By btnAlertas =By.xpath("//android.widget.TextView[@text='Alertas']");
	 protected By btnAlerta1 =By.xpath("//android.widget.Button[@resource-id='alertButton']");
	 protected By aceptar =By.xpath("//android.widget.Button[@text='Aceptar']");
	 protected By cancelar =By.xpath("//android.widget.Button[@text='Cancelar']");
	 protected By btnAlerta2 =By.xpath("//android.widget.Button[@resource-id='timerAlertButton']");
	 protected By btnAlerta3 =By.xpath("//android.widget.Button[@resource-id='confirmButton']");
	 protected By btnTexto =By.xpath("//android.widget.Button[@resource-id='promtButton']");
	 protected By texto =By.xpath("//android.widget.EditText[@resource-id='com.android.chrome:id/js_modal_dialog_prompt']");
	 protected By btnWidgets =By.xpath("//android.widget.TextView[@text='Widgets']");
	 protected By btnSelectordeFechas =By.xpath("//android.widget.TextView[@text='Selector de fechas']");
	 protected By Fecha1 =By.xpath("//android.widget.EditText[@resource-id='datePickerMonthYearInput']");
	 protected By Fecha2 =By.xpath("//android.widget.EditText[@resource-id='dateAndTimePickerInput']");
	 
	 
	 
	 
	 
}
