package com.testNG;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;


import com.ClaseBase.ClasesBase;
import com.PagesObjets.PageObjectDemo;
import com.PagesObjets.PageObjectMercadoMobile;
import com.PagesObjets.PagsObjetCalculadoraOperaciones;
import com.utilidades.GenerarReportePdf;
import com.utilidades.MyScreenRecorder;
import com.utilidades.Utilidades;

import io.appium.java_client.AppiumDriver;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.io.File;




public class RunTestNG 
{
 
	@SuppressWarnings("rawtypes")
	private AppiumDriver driver;
	ClasesBase claseBase;
	PagsObjetCalculadoraOperaciones paginaOperaciones;
	PageObjectMercadoMobile Objeto;
	PageObjectDemo Demoqa;
	GenerarReportePdf generarReportePdf;
	String rutaImgEncabezadoML,rutaImgEncabezadoMT,rutaImgEncabezadoTQA,rutaImgEncabezadoCal,platformNameCL,deviceNameCL,platformVersionCL,
	noResetCL,autoGrantPermissionsCL;
	
  @SuppressWarnings("unchecked")
@BeforeClass
  @Parameters({"rutaImgML","rutaImgMT","rutaImgTQA","rutaImgCal","platformName","deviceName","platformVersion","noReset","autoGrantPermissions"})
  public void beforeClass(@Optional("default") String rutaImgML,
		  				  @Optional("default") String rutaImgMT,
		  				  @Optional("default") String rutaImgTQA,
		  				  @Optional("default") String rutaImgCal,
		  				  @Optional("default") String platformName,
		  				  @Optional("default") String deviceName,
		  				  @Optional("default") String platformVersion,
		  				  @Optional("default") String noReset,
		  				  @Optional("default") String autoGrantPermissions) throws Exception
  {
	  System.out.println(rutaImgMT);
	  System.out.println(rutaImgTQA);
	 
	
	//INSTANCIAR CLASE REPORTES
	generarReportePdf = new GenerarReportePdf();
	//GRABAR EN LA VARIABLE LA RUTA
	rutaImgEncabezadoML=rutaImgML;
	//GRABAR EN LA VARIABLE LA RUTA
	Objeto =new PageObjectMercadoMobile(driver);
	Demoqa =new PageObjectDemo(driver);
	
	rutaImgEncabezadoMT=rutaImgMT;
	rutaImgEncabezadoTQA=rutaImgTQA;
	rutaImgEncabezadoCal=rutaImgCal;
	platformNameCL=platformName;
	deviceNameCL=deviceName;
	platformVersionCL=platformVersion;
	noResetCL=noReset;
	autoGrantPermissionsCL=autoGrantPermissions;
  }
  
  @DataProvider(name="calculadora")
  public Object[][] datosCL() throws Exception
  {
	  try {
		  Object[][] arreglo=Utilidades.getTableArray("./Data/DataTest.xlsx", "calculadora");
		  return arreglo;
	} catch (Exception e) {
		System.out.println(e);
	}
	return null;
	  
  }  
  
  @DataProvider(name="demoQA")
  public Object[][] datosQA() throws Exception
  {
	  try {
		  Object[][] arreglo=Utilidades.getTableArray("./Data/DataTest.xlsx", "toolsQA");
		  return arreglo;
	} catch (Exception e) {
		System.out.println(e);
	}
	return null;
	  
  }  
 
  @DataProvider(name="mercadolibre")
  public Object[][] datos() throws Exception
  {
	  try {
		  Object[][] arreglo=Utilidades.getTableArray("./Data/DataTest.xlsx", "mercadolibre");
		  return arreglo;
	} catch (Exception e) {
		System.out.println(e);
	}
	return null;
	  
  }
  /*
  
  @SuppressWarnings("unchecked")
  @Test(dataProvider = "calculadora",priority=1)
  public void calculadoraTestNG(String num1,String operacion,String num2,String appPackage,String appActivity,String generarReporte,String run) throws Exception 
  {
		driver= ClasesBase.appiumDriverConnetion(platformNameCL,deviceNameCL,platformVersionCL,appPackage,appActivity,noResetCL,autoGrantPermissionsCL);

	  	if(run.equals("Si")) {
	  		
	  		
	  		claseBase = new ClasesBase(driver);
	  		paginaOperaciones = new PagsObjetCalculadoraOperaciones(driver);
	  		//OBTENER EL NOMBRE DEL METODO A EJECUTAR
			String nomTest = Thread.currentThread().getStackTrace()[1].getMethodName();
			//CREAR CARPETA PARA ALMACENAMIENTO DE IMAGENES
			File rutaCarpeta = claseBase.crearCarpeta(nomTest);
			
			
			if(generarReporte.equals("Si")) {
				    //GUARDA LA RUTA DE LA IMAGEN PARA EL ENCABEZADO DEL PDF
				    generarReportePdf.setRutaImagen(rutaImgEncabezadoCal);
					//INICIO DE LA GRABACION DEL VIDEO
					MyScreenRecorder.startRecording(nomTest, rutaCarpeta);
					//INICIA CREACION DE REPORTE PDF
					generarReportePdf.crearPlantilla(nomTest, rutaCarpeta);
					//ACCEDER A LA CALCULADORA
					paginaOperaciones.OperacionSuma(num1,operacion,num2,rutaCarpeta,generarReporte );
					
					//FINALIZA GRABACION DE VIDEO
					MyScreenRecorder.stopRecording();
					//INICIA CREACION DE REPORTE PDF
					generarReportePdf.cerrarPalntilla();
			}else {
				
			}
	  	}else {
	  		System.out.println("No a Seleccionado esta automatizacion");
	  	}
  }
  
  @Test(dataProvider = "mercadolibre",priority=3)
  public void mercadolibreCrearCuentaTestNG(String url,String email,String generarReporte,String run) throws Exception 
  {
	  	if(run.equals("Si")) {
	  		//OBTENER EL NOMBRE DEL METODO A EJECUTAR
			String nomTest = Thread.currentThread().getStackTrace()[1].getMethodName();
			//CREAR CARPETA PARA ALMACENAMIENTO DE IMAGENES
			File rutaCarpeta = claseBase.crearCarpeta(nomTest);
	  		if(generarReporte.equals("Si")) {
			    //GUARDA LA RUTA DE LA IMAGEN PARA EL ENCABEZADO DEL PDF
			    generarReportePdf.setRutaImagen(rutaImgEncabezadoML);
				//INICIO DE LA GRABACION DEL VIDEO
				MyScreenRecorder.startRecording(nomTest, rutaCarpeta);
				//INICIA CREACION DE REPORTE PDF
				generarReportePdf.crearPlantilla(nomTest, rutaCarpeta);
				//CREAR UNA CUENTA
				Objeto.CompraM(rutaCarpeta, nomTest, email, url, nomTest);
				//FINALIZA GRABACION DE VIDEO
				MyScreenRecorder.stopRecording();
				//INICIA CREACION DE REPORTE PDF
				generarReportePdf.cerrarPalntilla();
	  		}else {
	  			 
	  		}
	  	}else {
	  		
	  		System.out.println("No a Seleccionado esta automatizacion");
	  	}
  }
 */ 
  @Test(dataProvider = "demoQA",priority=1)
  public void demoAlertasWith(String url,String cuadrotexto,String fecha1,String fecha2 , String evidencias,String run,String appPackage, String appActivity ) throws Exception 
  {
	  driver= ClasesBase.appiumDriverConnetion(platformNameCL,deviceNameCL,platformVersionCL,appPackage,appActivity,noResetCL,autoGrantPermissionsCL);
	  
	  	if(run.equals("Si")) {
	  		//OBTENER EL NOMBRE DEL METODO A EJECUTAR
			String nomTest = Thread.currentThread().getStackTrace()[1].getMethodName();
			//CREAR CARPETA PARA ALMACENAMIENTO DE IMAGENES
			File rutaCarpeta = claseBase.crearCarpeta(nomTest);
	  		if(evidencias.equals("Si")) {
			    //GUARDA LA RUTA DE LA IMAGEN PARA EL ENCABEZADO DEL PDF
			    generarReportePdf.setRutaImagen(rutaImgEncabezadoTQA);
				//INICIO DE LA GRABACION DEL VIDEO
				MyScreenRecorder.startRecording(nomTest, rutaCarpeta);
				//INICIA CREACION DE REPORTE PDF
				generarReportePdf.crearPlantilla(nomTest, rutaCarpeta);
				//CREAR UNA CUENTA
				Demoqa.Demoalerta(rutaCarpeta, url,cuadrotexto,fecha1,fecha2, evidencias);
				//FINALIZA GRABACION DE VIDEO
				MyScreenRecorder.stopRecording();
				//INICIA CREACION DE REPORTE PDF
				generarReportePdf.cerrarPalntilla();
	  		}else {
	  			 
	  		}
	  	}else {
	  		
	  		System.out.println("No a Seleccionado esta automatizacion");
	  	}
  }
  
 
  @AfterClass
  public void afterClass() 
  {
	  driver.quit();
  }

}
