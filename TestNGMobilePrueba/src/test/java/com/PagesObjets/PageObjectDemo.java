package com.PagesObjets;

import java.io.File;


import com.MapsObjet.MapsObjectDemo;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;



public class PageObjectDemo extends MapsObjectDemo
{

	
	public PageObjectDemo(AppiumDriver<MobileElement>driver) {
		super(driver);
		this.driver =(AppiumDriver<MobileElement>) driver;
		// TODO Auto-generated constructor stub
	}

	public void Demoalerta( File rutaCarpeta, String nomTest,String url, String cuadrotexto, String fecha1, String fecha2) throws Exception ,InterruptedException
	{
		try 
		{
			
			
			//INCIO DE BUSQUEDA GOOGLE
			click (btnGoogle,rutaCarpeta,nomTest);
			//INGRESA URL PAGINA
			sendkey(url,btnGoogle,rutaCarpeta,nomTest);
			tiempoEspera(2000);
            enter(btnGoogle,rutaCarpeta, nomTest);
            //SCROLL EN LA PAGINA 
            scrollVertical(rutaCarpeta,532,1400,0,4);
            scrollVertical(rutaCarpeta,532,1400,0,4);
            scrollVertical(rutaCarpeta,532,1000,0,4);
            tiempoEspera(2000);
            //INGRESA A OPCION DE ALERTAS 
            click (btnAlertasMV,rutaCarpeta,nomTest);
            tiempoEspera(2000);
            scrollVertical(rutaCarpeta,532,540,1400,3);
            tiempoEspera(2000);
            scrollVertical(rutaCarpeta,532,540,1400,3);
            scrollVertical(rutaCarpeta,532,540,1400,3);
            tiempoEspera(2000);
            //BUSCA BOTON ALERTAS 
            click (btnAlertas,rutaCarpeta,nomTest);
            scrollVertical(rutaCarpeta,532,540,1400,3);
            tiempoEspera(5000);
            // ALERTA 1 ACEPTAR 
            click (btnAlerta1,rutaCarpeta,nomTest);
            click (aceptar,rutaCarpeta,nomTest);
            scrollVertical(rutaCarpeta,532,1000,0,4);
            //ALERTA 2, 5 SEGUNOS ACEPTAR 
            click (btnAlerta2,rutaCarpeta,nomTest);
            tiempoEspera(6000);
            click (aceptar,rutaCarpeta,nomTest);
            //ALERTA 3 ACEPTAR
            click (btnAlerta3,rutaCarpeta,nomTest);
            click (aceptar,rutaCarpeta,nomTest);
            //ALERT 3 CANCELAR 
            click (btnAlerta3,rutaCarpeta,nomTest);
            click (cancelar,rutaCarpeta,nomTest);
            //ALERTA 4 INGRESAR TEXTO 
            click (btnTexto,rutaCarpeta,nomTest);
            sendkey(cuadrotexto,texto,rutaCarpeta,nomTest);
            click (aceptar,rutaCarpeta,nomTest);
            click (btnGoogle,rutaCarpeta,nomTest);
			//INGRESA URL PAGINA
			sendkey(url,btnGoogle,rutaCarpeta,nomTest);
			tiempoEspera(2000);
            enter(btnGoogle,rutaCarpeta, nomTest);
            scrollVertical(rutaCarpeta,532,1400,0,4);
            scrollVertical(rutaCarpeta,532,1400,0,4);
            scrollVertical(rutaCarpeta,532,1400,0,4);
            tiempoEspera(2000);
            //SELECCIONAR WIDGETS
            click (btnWidgets,rutaCarpeta,nomTest);
            tiempoEspera(2000);
            scrollVertical(rutaCarpeta,532,540,1400,3);
            //BOTON SELECTOR DE FECHAS
            click (btnSelectordeFechas,rutaCarpeta,nomTest);
            scrollVertical(rutaCarpeta,532,540,1400,3);
            tiempoEspera(5000);
            //FECHA 1 
            click (Fecha1,rutaCarpeta,nomTest);
            borrar (Fecha1, rutaCarpeta, nomTest);
            sendkey(fecha1,Fecha1,rutaCarpeta,nomTest);
            enter(Fecha1,rutaCarpeta, nomTest);
            //FECHA 2 
            click (Fecha2,rutaCarpeta,nomTest);
            borrar (Fecha2, rutaCarpeta, nomTest);
            sendkey(fecha2,Fecha2,rutaCarpeta,nomTest);
           
            
           
            
            
			 
		} 
		catch (Exception e)
        {
			System.out.println(e);
		}
		
		
	  }
	}

	
	

	
	

