package MapsObjets;

import org.openqa.selenium.By;
import ClaseBase.ClaseBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class MapsObjectSuma extends ClaseBase
{
	 public MapsObjectSuma(AppiumDriver<MobileElement>driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	 
	 
	 protected By btnNumero =By.xpath("//android.widget.Button[@text='{0}']");
	 protected By btnOperaciones =By.xpath("//android.widget.ImageView[@content-desc='{0}']");
	 protected By btnIgual =By.id("com.android.calculator2:id/eq");
	 protected By btnLimpiar =By.id("com.android.calculator2:id/op_clr");
	 protected By btnGoogle =By.id("com.android.chrome:id/url_bar");
	 protected By btnBusqueda =By.xpath("//android.widget.Button[@text='Buscar']");
	 protected By btnBusqueda2 =By.xpath("//android.widget.EditText[@resource-id='cb1-edit']");
	 protected By Producto =By.xpath("(//android.widget.TextView)[6]");
	 protected By Comprar =By.xpath("//android.widget.Button[@text='Comprar ahora']");
	 protected By Crear =By.xpath("//android.view.View[@content-desc='Crear cuenta']/android.widget.TextView");
	 protected By Nombre =By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View/android.widget.EditText");
	 protected By Apellido =By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[2]/android.view.View/android.view.View/android.view.View/android.widget.EditText");
	 protected By dni =By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[3]/android.view.View[3]/android.view.View/android.view.View/android.widget.EditText");
	 protected By correo =By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View[2]/android.view.View/android.view.View[4]/android.view.View/android.view.View/android.view.View/android.widget.EditText");
	 
	 
	 
	 
}
