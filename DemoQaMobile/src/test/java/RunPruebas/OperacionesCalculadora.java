package RunPruebas;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ClaseBase.ClaseBase;
import PagesObjets.PageObjectAlibaba;
import io.appium.java_client.AppiumDriver;
import urilidadesExcel.ReadExcelFile;
import urilidadesExcel.WriteExcelFile;

public class OperacionesCalculadora
{
	@SuppressWarnings("rawtypes")
	// CREAR OBJETO TIPO WEBDRIVER
    private AppiumDriver driver;
    Properties propiedades;
    ReadExcelFile leer;
    WriteExcelFile escribir;
    InputStream entrada=null;
    ClaseBase claseBase;
    PageObjectAlibaba Ali;
    
   
    

   // CREAR ANOTACIONES JUNIT
    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception
    {
    	//INSTANCIAR LA CLASE PROPIEDADES DE JAVA
    	propiedades =new Properties();
    	claseBase =new ClaseBase(driver);
    	driver=ClaseBase.appiumdriverConection(leer,propiedades,"capability",0,1);
    	Suma =new PageObjectCalculadora(driver);
    	Ali =new PageObjectAlibaba(driver);
    	
    	//INSTANCIAR CLASES DE EXCEL
    	leer=new ReadExcelFile();
    	escribir= new WriteExcelFile();
    	
        //CREAR VARIABLE  TIPO INPUTSTRING
    	InputStream entrada =null;
    	
    	//VALIDAR SI GENERA ERROR AL NO ENCONTRAR EL ARCHIVO 
    	try
    	{
    		entrada=new FileInputStream("./properties/propiedades");
    		propiedades.load(entrada);
    	}
    	catch(FileNotFoundException e)
    	{
    		e.printStackTrace();
    		System.out.println(e);
    	}
 
       
     }
/*
 @Test
 public void Operaciones() throws Exception
 {
 	
 	//OBTENER EL NOMBRE DEL METODO A EJECUTAR
	 String nomTest =Thread.currentThread().getStackTrace()[1].getMethodName();
	//CREAR CARPETA  PARA ALMACENAMIENTO DE IMAGENES 
	 File rutaCarpeta =claseBase.crearCarpeta(propiedades,nomTest);
	 Suma.calculadora(leer, propiedades,rutaCarpeta, nomTest);
  
 }
*/ 
 
 

@SuppressWarnings("unchecked")
@Test
 public void GestionAlibaba() throws Exception
 {
 	driver=ClaseBase.appiumdriverConection(leer,propiedades,"capability",1,0);
 	
 	if(leer.getCellValue(propiedades.getProperty("FileInputStream"),"capability", 1, 0).equals("Si"))
 	{
 		
 		claseBase=new ClaseBase(driver);
 		Ali=new PageObjectAlibaba(driver);
 	//OBTENER EL NOMBRE DEL METODO A EJECUTAR
	 String nomTest =Thread.currentThread().getStackTrace()[1].getMethodName();
	//CREAR CARPETA  PARA ALMACENAMIENTO DE IMAGENES 
	 File rutaCarpeta =claseBase.crearCarpeta(propiedades,nomTest);
	 Ali.Alibaba(leer, propiedades,rutaCarpeta,nomTest);
 	}
 	else
 	{
 		
 	}
  
 }
 

 @After
    public void cerrarNavegador() {
         driver.close();
         driver.quit();
    }



}
