package com.testNG;

import org.testng.annotations.Test;

import com.Utilidades.ExcelUtilidades;

import ClaseBase.ClaseBase;
import PagesObjects.PagsObjectInicio;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;

public class EjemploTestNG 
{
	PagsObjectInicio mercadoLibre;
	ClaseBase claseBase;
	private WebDriver driver;
  
  
  @BeforeClass
  public void beforeClass() 
  {
	//ASIGNAMOS LAS OPCIONES Y LA CONFIGURACION DEL NAVEGADOR A LA VARIABLE DRIVER
	driver = ClaseBase.chromeDriverConnection();
	claseBase = new ClaseBase(driver);
	mercadoLibre = new PagsObjectInicio(driver);
  }
  
  
  @DataProvider(name = "busqueda")
  public Object[][] datos() throws Exception
  {
	  Object[][] arreglo=ExcelUtilidades.getTableArray("./Data/busqueda.xlsx", "mercadoLibre");
	  return arreglo;
  }
  
  @Test(dataProvider = "busqueda", description="acceso a periferia")
  
  public void pruebaInicialTestNG(String ejecutarCaso, String correo, String url, String producto) throws Exception 
  {
	  if(ejecutarCaso.equals("Si")) {
		  //ACCEDER AL METODO DE ABRIR PAGINA
		  mercadoLibre.urlAcceso(url);
		  
		  //OBTENER EL NOMBRE DEL METODO A EJECUTAR
		  String nomTest = Thread.currentThread().getStackTrace()[1].getMethodName();
							
		  //CREAR CARPETA PARA ALMACENAMIENTO DE IMAGENES
		  File rutaCarpeta = ClaseBase.crearCarpeta(nomTest);
		  
		  //REALIZAR LA BUSQUEDA DEL PRODUCTO
		  mercadoLibre.busquedainicial(producto,rutaCarpeta);
		  
	  } else {
		  System.out.println("La automatizacion no se ejecutara");
	  }
	  
  }
	
  
  
  @AfterClass
  public void afterClass() 
  {
	  driver.close();
  }

}
