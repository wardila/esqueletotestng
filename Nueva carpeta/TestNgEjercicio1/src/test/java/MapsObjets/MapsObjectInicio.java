package MapsObjets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import ClaseBase.ClaseBase;

public class MapsObjectInicio extends ClaseBase
{
	//CONSTRUCTOR DE LA CLASE
	public MapsObjectInicio(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	//ELEMENTOS PAGINA PRINCIPAL
	protected By txtBusqueda = By.name("as_word");
	protected By linkUbicacion = (By.xpath("//button[@type='button' and @class='andes-tooltip-button-close']"));
	protected By btnCookie = (By.xpath("//div[@class='cookie-consent-banner-opt-out__actions']/button[text()='Entendido']"));
	protected By linkCelular = (By.xpath("//ol[@class='ui-search-layout ui-search-layout--stack']/li[1]/div/div/div[1]"));
	protected By botonAddCarrito = (By.xpath("//button/span[text()='Agregar al carrito']"));
	protected By btnCrearcuenta = (By.xpath("//span[text()='Crear cuenta']"));
	protected By btnCreaTuCuentaHome = (By.xpath("//*[@data-link-id=\"registration\"]"));
	
	
}
