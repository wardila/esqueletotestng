package MapsObjets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import ClaseBase.ClaseBase;



public class MapObjectCrearCuenta extends ClaseBase
{

	public MapObjectCrearCuenta(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	protected By btnCheckBox = (By.id("terms-and-conds"));
	protected By btnContinuar = (By.xpath("//span[@class='andes-button__content']"));
	protected By btnValidarEmail = (By.xpath("//span[text()='Validar']"));
	protected By txtEmail = By.name("email");
}
