package PagesObjects;

import java.io.File;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import MapsObjets.MapObjectCrearCuenta;
import utilidades.ReadExcelFile;

public class PagObjectCrearCuenta extends MapObjectCrearCuenta
{

	public PagObjectCrearCuenta(WebDriver driver) 
	{
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void crearCuenta(ReadExcelFile leer, Properties propiedades, File rutaCarpeta) throws Exception 
	{
		try {
			//CLICK EN BOTON "ACEPTAR  TERMINOS Y CONDICIONES"
			click(btnCheckBox, rutaCarpeta);
			//CLICK EN EL BOTON "CONTINUAR"
			click(btnContinuar, rutaCarpeta);
			//CLICK EN EL BOTON "VALIDAR-EMAIL"
			click(btnValidarEmail, rutaCarpeta);
			//SELECCIONAR EL INPUT
			click(txtEmail, rutaCarpeta);
			//ESCRIBIR UN VALOR
			sendkey(leer.getCellValue(propiedades.getProperty("filePathExcel"), "mercadolibre", 1, 1),txtEmail, rutaCarpeta);
			//TIEMPO DE ESPERA
			tiempoEspera(2000);
		}catch(InterruptedException e){
			
			System.out.println(e);
			
		}
	}
}
