package PagesObjects;


import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebDriver;

import MapsObjets.MapsObjectInicio;


public class PagsObjectInicio extends MapsObjectInicio
{
	
	
	//CONSTRUCTOR DE LA CLASE
	public PagsObjectInicio(WebDriver driver) 
	{
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	//METODO INICIAL
	public void urlAcceso(String url) throws IOException
	{
		driver.get(url);
		//BusquedaInicial();
	}
	
	
	//METODO PRIMERA PRUEBA 
	public void busquedainicial(String producto,File rutaCarpeta) throws Exception 
	{
				
		try {
			//SELECCIONAR EL INPUT
			click(txtBusqueda, rutaCarpeta);
			//ENVIAR UN VALOR A BUSCAR
			sendkey(producto,txtBusqueda,rutaCarpeta);
			//ACEPTAMOS LA BUSQUEDA
			submit(txtBusqueda,rutaCarpeta);
			//TIEMPO DE ESPERA
			tiempoEspera(2000);
			//CERRAR VENTANA DE UBICACION
			click(linkUbicacion,rutaCarpeta);
			//CERRAR COOKIES
			click(btnCookie, rutaCarpeta);
			//INGRESAR AL PRIMER ELEMENTO DE RESULTADO
			click(linkCelular, rutaCarpeta);
			//TIEMPO DE ESPERA
			tiempoEspera(2000);
			//CLICK EN ADD CARRITO
			click(botonAddCarrito, rutaCarpeta);
			//CLICK EN EL BOTON "CREAR CUENTA"
			click(btnCrearcuenta, rutaCarpeta);
		}catch(InterruptedException e){
			
			System.out.println(e);
			
		}
	}
	
	//CREAR CUENTA DESDE EL HOME
	public void homeCrearCuenta (File rutaCarpeta) throws Exception 
	{
		try {
			//CERRAR COOKIES
			click(btnCookie, rutaCarpeta);
			//SELECCIONAMOS CREAR CUENTA
			click(btnCreaTuCuentaHome, rutaCarpeta);
		}catch(InterruptedException e){
					
				System.out.println(e);
					
			}
		}
	
}