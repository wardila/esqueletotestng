import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import CaseBase.ClaseBase;
import PageObjects.PageObjectCrearCuenta;
import PageObjects.PagsObjectInicio;
import utilidadesExcel.ExcelUtilidades;
import utilidadesExcel.MyScreenRecorder;

public class EjemploTestNG
{
	
  PagsObjectInicio paginaPrincipal;
  PageObjectCrearCuenta mercadolibre;
  ClaseBase clasebase;
  
  
  private WebDriver driver;
  
  @BeforeClass
  public void beforeClass()
  {
	  
	  driver=ClaseBase.chomeDriverConnetion();
	  clasebase =new ClaseBase(driver);
	  paginaPrincipal =new PagsObjectInicio(driver);
	  mercadolibre=new PageObjectCrearCuenta(driver);
	
  }
  
  @DataProvider(name = "mercadolibre")
  public Object [][] datos () throws Exception
  {
	  Object[][] arreglo=ExcelUtilidades.getTableArray("./Data/mercadolibre.xlsx", "registro");
	  return arreglo;
  }
  

  @Test(dataProvider ="mercadolibre")
  public void pruebaInicialTestNG(String ejecutarcaso,String url,String producto,String correo)throws Exception
  {
	  
	  if (ejecutarcaso.equals("Si"))
	  {
		//OBTENER EL NOMBRE DEL ETODO A EJECUTAR 
		  String nomTest= Thread.currentThread().getStackTrace()[1].getMethodName();
		 
		  //ACCEDER AL METODO DE ABRIR PAGINA 
		  paginaPrincipal.urlAcceso(url);
		  
		  //CREAR CARPETA DE ALMCENAMIENTO IMAGENES 
		  File rutaCarpeta =clasebase.crearCarpeta(nomTest);
		  paginaPrincipal.compraMercadoLibre(rutaCarpeta,producto);
		  mercadolibre.ComprarProducto(rutaCarpeta,correo);
		 
		  
	  }
	  else 
		  System.out.println("La automatizacion no empezara ");
	
	 
  }
  
  @AfterClass
  public void afetrClass()
  {
	  driver.close();
  }
  
}


