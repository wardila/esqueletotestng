package PageObjects;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;

import MapsObjects.MapObjectCrearCuenta;


public class PageObjectCrearCuenta extends MapObjectCrearCuenta

 {
	public PageObjectCrearCuenta(WebDriver driver)
	{
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void ComprarProducto( File rutaCarpeta,String correo) throws Exception
	{
		
	 click(cookies, rutaCarpeta);
	 tiempoEspera(2000);
	        
	 click(cancelarUbicacion, rutaCarpeta);
	 click(btnUniversal, rutaCarpeta);
	//CREAR UN OBJETO PARA CREAR CUENTA 
	// CREA UN OBJETO QUE PARA LA CREAR CUENTA
	 click(btncomprarAhora, rutaCarpeta);
	 click(crearCuenta, rutaCarpeta);
    
    // CREA UN OBJETO ACEPTAR LAS COOKIS
    click(checkAutorizo, rutaCarpeta);
    
    //DAR CLICK A CONTINUAR
    click(Continuar, rutaCarpeta);
    
    //CLICK VALIDAR CORREO
    click(confirmarCorreo, rutaCarpeta);
    
    // INGRESAR CORREO
    clear(txtCorreo, rutaCarpeta);
    sendKey(txtCorreo ,correo, rutaCarpeta);
    tiempoEspera(2000);
	}
	
	public void ccreartuCuenta( Properties propiedades) throws Exception, IOException
	{
		//OBTENER EL NOMBRE DEL METODO A EJECUTAR
		   String nomTest =Thread.currentThread().getStackTrace()[1].getMethodName();
		   //CREAR CARPETA  PARA ALMACENAMIENTO DE IMAGENES 
		   File rutaCarpeta =crearCarpeta(nomTest);
		   
		click(cookies, rutaCarpeta);
		click(creatuCuenta, rutaCarpeta);
		click(checkAutorizo, rutaCarpeta);
		click(Continuar, rutaCarpeta);
		click(confirmarCorreo, rutaCarpeta);
		    
		    // INGRESAR CORREO
		 clear(txtCorreo, rutaCarpeta);
		 sendKey(txtCorreo, "wardila0012@gmail.com", rutaCarpeta);
		 tiempoEspera(2000);
	}

	
		
	}

	


