package RunPruebas;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import ClasBase.ClaseBase;
import PagesObjets.PageObjectWith;
import PagesObjets.PagsObjtDemoQAPage;
import urilidadesExcel.ReadExcelFile;
import urilidadesExcel.WriteExcelFile;

public class runPrueba
{
	// CREAR OBJETO TIPO WEBDRIVER
    private WebDriver driver;
    PagsObjtDemoQAPage demoqaPage;
    Properties propiedades;
    ReadExcelFile leer;
    WriteExcelFile escribir;
    InputStream entrada=null;
    ClaseBase claseBase;
    PageObjectWith demoWith;
    PageObjectWith demoWith2;
    

   // CREAR ANOTACIONES JUNIT
    @Before
    public void setUp() throws IOException
    {
    	//INSTANCIAR LA CLASE PROPIEDADES DE JAVA
    	demoWith=new PageObjectWith(driver);
    	demoWith2=new PageObjectWith(driver);
    	demoqaPage=new PagsObjtDemoQAPage(driver);
    	propiedades =new Properties();
    	claseBase =new ClaseBase(driver);
    	driver=ClaseBase.chomeDriverConnetion(leer,propiedades);
    	//INSTANCIAR CLASES DE EXCEL
    	leer=new ReadExcelFile();
    	escribir= new WriteExcelFile();
        //CREAR VARIABLE  TIPO INPUTSTRING
    	InputStream entrada =null;
    	//VALIDAR SI GENERA ERROR AL NO ENCONTRAR EL ARCHIVO 
    	try
    	{
    		entrada=new FileInputStream("./properties/propiedades");
    		propiedades.load(entrada);
    	}
    	catch(FileNotFoundException e)
    	{
    		e.printStackTrace();
    		System.out.println(e);
    	}
 
       
     }
    /*
 @Test
    public void QADemo() throws Exception
    {
    	//ACCESO A PAGINA WEB
	 demoqaPage.urlAcceso(propiedades.getProperty("url"));
    	
    	//OBTENER EL NOMBRE DEL METODO A EJECUTAR
		   String nomTest =Thread.currentThread().getStackTrace()[1].getMethodName();
		   //CREAR CARPETA  PARA ALMACENAMIENTO DE IMAGENES 
		   File rutaCarpeta =claseBase.crearCarpeta(propiedades,nomTest);
	   demoqaPage.DeMoQA(leer, propiedades,rutaCarpeta);
     
    }

 @Test
 public void QAWith() throws Exception
 {
 	//ACCESO A PAGINA WEB
	 demoqaPage.urlAcceso(propiedades.getProperty("url"));
 	
 	//OBTENER EL NOMBRE DEL METODO A EJECUTAR
		   String nomTest =Thread.currentThread().getStackTrace()[1].getMethodName();
		   //CREAR CARPETA  PARA ALMACENAMIENTO DE IMAGENES 
		   File rutaCarpeta =claseBase.crearCarpeta(propiedades,nomTest);
		   demoWith.DeMoWithQA(leer, propiedades,rutaCarpeta);
  
 }
  */   
  
 @Test
 public void QAWith2() throws Exception
 {
 	//ACCESO A PAGINA WEB
	 demoqaPage.urlAcceso(propiedades.getProperty("url"));
 	
 	//OBTENER EL NOMBRE DEL METODO A EJECUTAR
		   String nomTest =Thread.currentThread().getStackTrace()[1].getMethodName();
		   //CREAR CARPETA  PARA ALMACENAMIENTO DE IMAGENES 
		   File rutaCarpeta =claseBase.crearCarpeta(propiedades,nomTest);
		   demoWith.DeMoWithQA2(leer, propiedades,rutaCarpeta);
  
 }
 

 
 

 @After
    public void cerrarNavegador() {
         driver.close();
         driver.quit();
    }



}
