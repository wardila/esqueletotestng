package PagesObjets;

import java.io.File;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import MapsObjets.MapsObjectQA;
import urilidadesExcel.ReadExcelFile;

public class PagsObjtDemoQAPage extends MapsObjectQA
{
	public PagsObjtDemoQAPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	 // METODO INICIAL
    public void urlAcceso(String url)
    {
        driver.get(url);
        // busquedaInicial();
     }
    
    public void palabra(String pal)
    {
        driver.get(pal);
        // busquedaInicial();
     }
    
    

	public void DeMoQA(ReadExcelFile leer, Properties propiedades, File rutaCarpeta) throws Exception
	{
		try 
		{
			 //BOTON INICIAL DE ALERTAS-FRAME-WINDOWS
			JavascriptExecutor js = (JavascriptExecutor) driver;        
		    js.executeScript("window.scrollBy(0,200)");
		    tiempoEspera(2000);
		     
		     //BOTON INICIAL DE ALERTA 
		     click (btnAlertFW, rutaCarpeta);  
			    js.executeScript("window.scrollBy(0,200)");
			//BOTON POPUP ALERTA
		     tiempoEspera(2000);
			 click(btnAlert, rutaCarpeta);
			 js.executeScript("window.scrollBy(0,200)");
			 tiempoEspera(2000);
			//CREACION DE LISTA 
		     List<WebElement> boton = listaElementos(botones, rutaCarpeta);
		     tiempoEspera(2000);
		     //ACCEDEMOS A LA PRIMERA ALERTA 
		     boton.get(0).click();
		     tiempoEspera(2000);
			 driver.switchTo().alert().accept();
			 tiempoEspera(2000);
			 captureScreen(rutaCarpeta);
			 
			 
			//ACCEDEMOS A LA SEGUNDA ALERTA 
		     boton.get(1).click();
		     tiempoEspera(6000);
			 driver.switchTo().alert().accept();
			 tiempoEspera(2000);
			 captureScreen(rutaCarpeta);
			 
			//ACCEDEMOS A LA TERCERA ALERTA 
		     boton.get(2).click();
		     tiempoEspera(2000);
			 driver.switchTo().alert().accept();
			 tiempoEspera(2000);
			 captureScreen(rutaCarpeta);
			 
			//ACCEDEMOS A LA TERCERA ALERTA CANCELAR
		     boton.get(2).click();
		     tiempoEspera(2000);
			 driver.switchTo().alert().dismiss();
			 tiempoEspera(2000);
			 captureScreen(rutaCarpeta);
			 
			//ACCEDEMOS A LA TERCERA ALERTA 
		     boton.get(3).click();
		     tiempoEspera(2000);
			 driver.switchTo().alert().sendKeys(propiedades.getProperty("pal"));
			 driver.switchTo().alert().accept();
			 tiempoEspera(6000);
			 captureScreen(rutaCarpeta);
		
				}
		catch (Exception e)
        {
			System.out.println(e);
		}
	 
	
	}
	
	
	
}
