package PagesObjets;

import java.io.File;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import MapsObjets.MapsObjectQA;
import urilidadesExcel.ReadExcelFile;


public class PageObjectWith extends MapsObjectQA
{

	
	public PageObjectWith(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void DeMoWithQA(ReadExcelFile leer, Properties propiedades, File rutaCarpeta) throws Exception
	{
		try 
		{
			 //BOTON INICIAL DE ALERTAS-FRAME-WINDOWS
			JavascriptExecutor js = (JavascriptExecutor) driver;        
		    js.executeScript("window.scrollBy(0,200)");
		    tiempoEspera(2000);
		    
		    //BOTON INICIAL DE ALERTA 
		     click (btnWith, rutaCarpeta);  
			    js.executeScript("window.scrollBy(0,200)");
			    
			 //BOTON POPUP DATE PICKER
			  tiempoEspera(2000);
			  js.executeScript("window.scrollBy(0,200)");
			  click(btnDatePicker, rutaCarpeta);
			  js.executeScript("window.scrollBy(0,200)");
			  tiempoEspera(2000);
			  //CLICK EN BARRA DE FECHA 1
			  click(btnFecha1, rutaCarpeta);
			  tiempoEspera(2000);
			  //SELECCIONA MES FECHA 1
			  List<WebElement> mes = listaElementos(selecMes, rutaCarpeta);
			  //INGRESAR MES 
			  mes.get(7).click();
			  tiempoEspera(2000);
			  //SELECCIONA DIA 
			  click(btnDia1, rutaCarpeta);
			  //SELECCION FECHA 2
			  click(btnFecha2, rutaCarpeta);
			  //BOTON MES 
			  click(btnMeses, rutaCarpeta);
			  //BOTON DIA 2 
			  click(btnDia2, rutaCarpeta);
			  //BOTON HORA
			  click(btnHora2, rutaCarpeta);
		      click(afora, rutaCarpeta);
			  tiempoEspera(5000);
		
		} 
		catch (Exception e)
        {
			System.out.println(e);
		}
		
		
	}

	
	public void DeMoWithQA2(ReadExcelFile leer, Properties propiedades, File rutaCarpeta) throws Exception
	{
		try 
		{
			 //BOTON INICIAL DE ALERTAS-FRAME-WINDOWS
			JavascriptExecutor js = (JavascriptExecutor) driver;        
		    js.executeScript("window.scrollBy(0,200)");
		    tiempoEspera(2000);
		    
		    //BOTON INICIAL DE ALERTA 
		     click (btnWith, rutaCarpeta);  
			    js.executeScript("window.scrollBy(0,200)");
			    
			 //BOTON POPUP DATE PICKER
			  tiempoEspera(2000);
			  js.executeScript("window.scrollBy(0,200)");
			  click(btnDatePicker, rutaCarpeta);
			  js.executeScript("window.scrollBy(0,200)");
			  tiempoEspera(2000);
			  //CLICK EN BARRA DE FECHA 1
			  click(btnFecha1, rutaCarpeta);
			  tiempoEspera(2000);
			  
			  
			  //LLAMADO DE FECHA 1 DEL SISTEMA
			  
			  String fecha=fechaQA();
			  String[] fechaV =fecha.split("-");
			  //INGRESO DE VALORES 
			  
			  int dia =Integer.parseInt(fechaV[0]);
			  int mes=Integer.parseInt(fechaV[1]);
			  int anno=Integer.parseInt(fechaV[2]);
			 
			  
			  dia=dia-1;
			  mes=mes-1;
			  anno=anno-1;
			  //BORRA FECHA ANTERIOR 
			  clear(btnFecha1, rutaCarpeta);
			  String fechaMenor = dia+"/"+mes+"/"+anno;
			  //TRAE FECHA DE BUSQUEDA
			  sendKey(btnFecha1,fechaMenor,rutaCarpeta);
			  //CLICK AFUERA DEL CUADRO 
			  click(afora, rutaCarpeta);
		
			  tiempoEspera(2000);
			   //SELECCION FECHA 2
			  click(btnFecha2, rutaCarpeta);
			 
			 
			 Month meses= LocalDate.now().minusMonths(1).getMonth();
			  
			 String fecha2=fechaQA2();
			  String[] fech=fecha2.split("-");
			  //INGRESO DE VALORES 
			  
			  int dia2 =Integer.parseInt(fech[1]);
			  //int meses =Integer.parseInt(fech[0]);
			  int anno2=Integer.parseInt(fech[2]);
			  int hora=Integer.parseInt(fech[3]);
			 
			 
			  
			  dia2=dia2-1;
			  anno2=anno2-1;
			  hora=hora-1;
			  
			 //Month meses= LocalDate.now().minusMonths(1).getMonth();
			  
			  String fechafinal =meses+" "+dia2+" "+anno2+" "+hora+" ";
			  clear(btnFecha2, rutaCarpeta);
			  sendKey(btnFecha2,fechafinal,rutaCarpeta);
			  
			  
			  
		      click(afora, rutaCarpeta);
			  tiempoEspera(5000);
		
		} 
		catch (Exception e)
        {
			System.out.println(e);
		}
		
		
	}
	
}
	
	

