package PageObjects;

import java.io.File;


import org.openqa.selenium.WebDriver;

import MapsObjects.MapsObjectTours;


public class PageObjectTours extends MapsObjectTours
{
	public PageObjectTours(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
}
	
	// METODO INICIAL
    public void urlAcceso(String url) 
    {
    	try {driver.get(url);
			
		} catch (Exception e) {
			System.out.println(e);
			// TODO: handle exception
		}
    	
    }

	public void Registroviaje( File rutaCarpeta,String nombre,String apellido,String telefono,String email, String direccion, String ciudad, String provincia,String codigopostal,String pais, String userName, String password ) throws Exception
	{
	 //INGRESO DE PRIMER NOMBRE 
	 click(txtFirstName,rutaCarpeta);
	 sendKey(txtFirstName,nombre,rutaCarpeta);
	 //INGRESO DE SEGUNDO NOMBRE 	
	 sendKey(txtlastName,apellido, rutaCarpeta);
	 //INGRESO DE NUMERO TELEFONICO 	
	 sendKey(txtPhone, telefono, rutaCarpeta);
	 //INGRESO DE NUMERO EMAIL 
     sendKey(txtEemail,email, rutaCarpeta);
	 //INGRESO DE DIRECCION 	
     sendKey(txtDireccion,direccion, rutaCarpeta);
	 //INGRESO DE DIRECCION 
     sendKey(txtciudad, ciudad, rutaCarpeta);
	 //INGRESAR PROVINCIA
     sendKey(txtprovincia,provincia, rutaCarpeta);
	 tiempoEspera(2000);
	 //INGRESAR CODIGO POSTAL	
     sendKey(txtcodigoPostal,codigopostal, rutaCarpeta);
	 tiempoEspera(2000);
	 //SELECCION DE PAIS
	 sendKey(txtpais, pais,rutaCarpeta);
	 tiempoEspera(2000);
	 //CONFIRMAR USUARIO	
     sendKey(txtuuserName,userName,rutaCarpeta);
	 tiempoEspera(2000);
	 //INGRESAR PASSWORD
     sendKey(txtpassword,password, rutaCarpeta);
	 tiempoEspera(2000);
	 //CONFIRMAR PASSWORD	
     sendKey(txtconpassword,password, rutaCarpeta);
	 tiempoEspera(2000);
	 //BOTON ENVIAR
	 click(btnEnviar,rutaCarpeta);
	
	}
	
	
	
}
