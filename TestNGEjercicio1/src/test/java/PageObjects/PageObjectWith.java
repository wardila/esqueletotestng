package PageObjects;

import java.io.File;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import MapsObjects.MapsObjectQA;




public class PageObjectWith extends MapsObjectQA
{

	
	public PageObjectWith(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	public void DeMoWithQA2(File rutaCarpeta,String fecha1,String fecha2) throws Exception
	{
		try 
		{
			 //BOTON INICIAL DE ALERTAS-FRAME-WINDOWS
			JavascriptExecutor js = (JavascriptExecutor) driver;         
		    js.executeScript("window.scrollBy(0,200)");
		    tiempoEspera(2000);
		    
		    //BOTON INICIAL DE ALERTA 
		     click (btnWith, rutaCarpeta);  
			    js.executeScript("window.scrollBy(0,200)");
			    
			 //BOTON POPUP DATE PICKER
			  tiempoEspera(2000);
			  js.executeScript("window.scrollBy(0,200)");
			  click(btnDatePicker, rutaCarpeta);
			  js.executeScript("window.scrollBy(0,200)");
			  tiempoEspera(2000);
			  //CLICK EN BARRA DE FECHA 1
			  click(btnFecha1, rutaCarpeta);
			  tiempoEspera(2000);
			
			  //BORRA FECHA ANTERIOR 
			  clear(btnFecha1, rutaCarpeta);
			  clear(btnFecha1, rutaCarpeta);
			  //TRAE FECHA DE BUSQUEDA
			  sendKey(btnFecha1,fecha1,rutaCarpeta);
			  //CLICK AFUERA DEL CUADRO 
			  click(afora, rutaCarpeta);
		
			  tiempoEspera(2000);
			   //SELECCION FECHA 2
			  
			  clear(btnFecha2, rutaCarpeta);
			  sendKey(btnFecha2,fecha2, rutaCarpeta);
			  tiempoEspera(2000);
			  
			  
		      click(afora, rutaCarpeta);
			  tiempoEspera(5000);
		
		} 
		catch (Exception e)
        {
			System.out.println(e);
		}
		
		
	}
	
}
	
	

