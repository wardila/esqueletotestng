import java.io.File;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import CaseBase.ClaseBase;
import PageObjects.PageObjectCrearCuenta;
import PageObjects.PageObjectTours;
import PageObjects.PageObjectWith;
import PageObjects.PagsObjectInicio;
import PageObjects.PagsObjtDemoQAPage;
import utilidadesExcel.ExcelUtilidades;
import utilidadesExcel.GenerarPDF;
import utilidadesExcel.MyScreenRecorder;


public class EjemploTestNG
{
	
  PagsObjectInicio paginaPrincipal;
  PageObjectCrearCuenta mercadolibre;
  PageObjectWith with;
  PagsObjtDemoQAPage Alerta;
  PageObjectTours Tour;
  ClaseBase clasebase;
  GenerarPDF generarReportePdf;
  String rutaMercadoLibre;
  String rutaQA;
  String rutaMercury;
  
  
  
  private WebDriver driver;
  
  @BeforeClass
  @Parameters({"rutaImagenReporte","rutaImagenQA","rutaMercuryT"})
  public void beforeClass(@Optional("default")String rutaImagenReporte,
		  @Optional("default")String rutaImagenQA,@Optional("default")String rutaMercuryT)
  {
	  
	  driver=ClaseBase.chomeDriverConnetion(); 
	  clasebase =new ClaseBase(driver);
	  Alerta =new PagsObjtDemoQAPage(driver);
	  paginaPrincipal =new PagsObjectInicio(driver);
	  mercadolibre=new PageObjectCrearCuenta(driver);
	  with =new PageObjectWith(driver);
	  Tour =new PageObjectTours(driver);
	  generarReportePdf =new GenerarPDF();
	  rutaMercadoLibre =rutaImagenReporte;
	  rutaQA =rutaImagenQA;
	  rutaMercury =rutaMercuryT;
	  

	  
	
  }
  
  @DataProvider(name = "mercadolibre")
  public Object [][] datos () throws Exception
  {
	  Object[][] arreglo=ExcelUtilidades.getTableArray("./Data/mercadolibre.xlsx", "registro");
	  return arreglo;
  }
  
  

  @Test(dataProvider ="mercadolibre",priority =4)
  public void pruebaInicialTestNG(String ejecutarcaso,String evidencia,String url,String producto,String correo)throws Exception
  {
	  
	  if (ejecutarcaso.equals("Si"))
	  {
		  if (evidencia.equals("Si"))
		  {
		//OBTENER EL NOMBRE DEL ETODO A EJECUTAR 
		  String nomTest= Thread.currentThread().getStackTrace()[1].getMethodName();
	       //ACCEDER AL METODO DE ABRIR PAGINA 
		  paginaPrincipal.urlAcceso(url);
		  
		  //CREAR CARPETA DE ALMCENAMIENTO IMAGENES 
		  File rutaCarpeta =clasebase.crearCarpeta(nomTest);
		  //INSTANCIAR LA RUTA IMAGEN
		  generarReportePdf.setRutaImagen(rutaMercadoLibre);
		  //INICIAR GRABACION 
		  MyScreenRecorder.startRecording(nomTest, rutaCarpeta);
		  //INICIAR GENERACION DE PDF
		  generarReportePdf.crearPlantilla(nomTest,rutaCarpeta);
		  //INGRESO A PAGE INICIAL
		  paginaPrincipal.compraMercadoLibre(rutaCarpeta,producto);
		  //INGRESO A CUERPO DE PAGINA
		  mercadolibre.ComprarProducto(rutaCarpeta,correo);
		  //CERRAR REPORTE DE PDF
		  generarReportePdf.cerrarPalntilla();
		  //CERRAR GRABACION DE PANTALLA 
		  MyScreenRecorder.stopRecording();
		  
		  }
	  }
	  else 
		  System.out.println("La automatizacion no empezara ");
	
	 
  }
  

  
  @Test(dataProvider ="mercadolibre",priority =5)
  public void pruebaInicialTestNGRegistro(String ejecutarcaso,String evidencia,String url,String producto,String correo)throws Exception
  {
	  
	  if (ejecutarcaso.equals("Si"))
	  {
		  if (evidencia.equals("Si"))
		  {
		//OBTENER EL NOMBRE DEL ETODO A EJECUTAR 
		  String nomTest= Thread.currentThread().getStackTrace()[1].getMethodName();
	       //ACCEDER AL METODO DE ABRIR PAGINA 
		  paginaPrincipal.urlAcceso(url);
		  
		  //CREAR CARPETA DE ALMCENAMIENTO IMAGENES 
		  File rutaCarpeta =clasebase.crearCarpeta(nomTest);
		  generarReportePdf.setRutaImagen(rutaMercadoLibre);
		  //INICIAR GRABACION 
		  MyScreenRecorder.startRecording(nomTest, rutaCarpeta);
		  //INICIAR GENERACION DE PDF
		  generarReportePdf.crearPlantilla(nomTest,rutaCarpeta);
		 
		  //INGRESO A PAGINA PARA REGISTRO
		  mercadolibre.CreartuCuenta(rutaCarpeta,correo);
		  //CERRAR REPORTE DE PDF
		  generarReportePdf.cerrarPalntilla();
		  //CERRAR GRABACION DE PANTALLA 
		  MyScreenRecorder.stopRecording();
		  }
		  
	  }
	  else 
		  System.out.println("La automatizacion no empezara ");
	
  }  
	  
 
  @DataProvider(name = "demoQA2")
  public Object [][] datos2 () throws Exception
  {
	  Object[][] arreglo=ExcelUtilidades.getTableArray("./Data/DemoQA2.xlsx", "Hoja1");
	  return arreglo;
  }
    
  

	  @Test(dataProvider ="demoQA2",priority =3) 
	  
	  public void DemoQAAlerta(String ejecutarcaso,String evidencia,String url,String palabra,String fecha1,String fecha2)throws Exception
	  {
		  
		  if (ejecutarcaso.equals("Si"))
		  {
			  if (evidencia.equals("Si"))
			  {
			//OBTENER EL NOMBRE DEL ETODO A EJECUTAR 
			  String nomTest= Thread.currentThread().getStackTrace()[1].getMethodName();
		      
			  
			  //CREAR CARPETA DE ALMCENAMIENTO IMAGENES 
			  File rutaCarpeta =clasebase.crearCarpeta(nomTest);
			  generarReportePdf.setRutaImagen(rutaQA);
			  //INICIAR GRABACION 
			  MyScreenRecorder.startRecording(nomTest, rutaCarpeta);
			  
			  //INICIAR GENERACION DE PDF
			  generarReportePdf.crearPlantilla(nomTest,rutaCarpeta);
			  //INGRESO A PAGE INICIAL
			  
			//ACCEDER AL METODO DE ABRIR PAGINA 
			  Alerta.urlAcceso(url);
			
			  //INGRESO A CUERPO DE PAGINA
			  Alerta.DeMoQA(rutaCarpeta,palabra);
			  //CERRAR REPORTE DE PDF
			  generarReportePdf.cerrarPalntilla();
			  //CERRAR GRABACION DE PANTALLA 
			  MyScreenRecorder.stopRecording();
			  
			  }  
		  }
		  else 
			  System.out.println("La automatizacion no empezara ");
		
		 
	  }
	  
	 

	  @Test(dataProvider ="demoQA2",priority =2)
	  public void DemoWithQA(String ejecutarcaso,String evidencia,String url,String palabra,String fecha1,String fecha2)throws Exception
	  {
		   
		  if (ejecutarcaso.equals("Si"))
		  {
			//OBTENER EL NOMBRE DEL ETODO A EJECUTAR 
			  String nomTest= Thread.currentThread().getStackTrace()[1].getMethodName();
			  
			  if (evidencia.equals("Si"))
			  {
			
			  //CREAR CARPETA DE ALMCENAMIENTO IMAGENES 
			  File rutaCarpeta =clasebase.crearCarpeta(nomTest);
			  generarReportePdf.setRutaImagen(rutaQA);
			  //INICIAR GRABACION 
			  MyScreenRecorder.startRecording(nomTest, rutaCarpeta);
			  //INICIAR GENERACION DE PDF
			  generarReportePdf.crearPlantilla(nomTest,rutaCarpeta);
			//ACCEDER AL METODO DE ABRIR PAGINA 
			  
			  Alerta.urlAcceso(url);
			  //INGRESO A PAGE INICIAL
			  with.DeMoWithQA2(rutaCarpeta,fecha1,fecha2);
			 
			  //CERRAR REPORTE DE PDF
			  generarReportePdf.cerrarPalntilla();
			  //CERRAR GRABACION DE PANTALLA 
			  MyScreenRecorder.stopRecording();
			  } 
			 
		  }
		  else {
			  System.out.println("La automatizacion no empezara ");}
		  
		
		 
	  }
	  
	  
	  
	  @DataProvider(name = "Mercury")
	  public Object [][] datos3 () throws Exception
	  {
		  Object[][] arreglo=ExcelUtilidades.getTableArray("./Data/Mercury.xlsx", "Tours");
		  return arreglo;
	  }
	 
	  
	  @Test(dataProvider ="Mercury",priority =1)
	  public void Mercurytours(String ejecutarcaso,String evidencia,String url,String nombre,String apellido,String telefono,String email, String direccion, String ciudad, String provincia,String codigopostal,String pais, String userName, String password)throws Exception
	  {
		   
		  if (ejecutarcaso.equals("Si"))
		  {
			//OBTENER EL NOMBRE DEL ETODO A EJECUTAR 
			  String nomTest= Thread.currentThread().getStackTrace()[1].getMethodName();
			  
			  if (evidencia.equals("Si"))
			  {
			
			  //CREAR CARPETA DE ALMCENAMIENTO IMAGENES 
			  File rutaCarpeta =clasebase.crearCarpeta(nomTest);
			  generarReportePdf.setRutaImagen(rutaMercury);
			  //INICIAR GRABACION 
			  MyScreenRecorder.startRecording(nomTest, rutaCarpeta);
			  //INICIAR GENERACION DE PDF
			  generarReportePdf.crearPlantilla(nomTest,rutaCarpeta);
			//ACCEDER AL METODO DE ABRIR PAGINA 
			  
			  Tour.urlAcceso(url);
			  //INGRESO A PAGE INICIAL
			  Tour.Registroviaje(rutaCarpeta,nombre,apellido,telefono,email,direccion,ciudad, provincia,codigopostal,pais,userName,password);
			 
			  //CERRAR REPORTE DE PDF
			  generarReportePdf.cerrarPalntilla();
			  //CERRAR GRABACION DE PANTALLA 
			  MyScreenRecorder.stopRecording();
			  } 
			 
		  }
		  else {
			  System.out.println("La automatizacion no empezara ");}
		  
		
		 
	  }
	  
 
  @AfterClass
  public void afetrClass()
  {
	  driver.close();
  }
  
}


