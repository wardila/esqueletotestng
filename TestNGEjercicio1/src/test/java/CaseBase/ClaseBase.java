package CaseBase;

import java.io.File;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import utilidadesExcel.GenerarPDF;



public class ClaseBase {
	
	protected static WebDriver driver;
     //CONSTRUCTOR DE CLASE
	    public ClaseBase(WebDriver driver)
	    {
	        super(); 
	    }
      //METODO DE NAVEGADOR
	    
	   
	    public static  WebDriver chomeDriverConnetion()
	    {
	        
	    	//SETEAR LAS PROPIEDADES DEL EJECUTABLE DE CHROME
	        System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe"); 
	        //DECLARAR EL OBJETO DRIVER TIPO CHROMEDRIVER
	        driver = new ChromeDriver();
	        
	        //MAXIMIZAR VENTANA DE NAVEGADOR
	        driver.manage().window().maximize();
	        
	        //INGRESAR LA URL DE PAGINA DE ACCESO
	        
	        return driver;
	      
	    }
	    
	    //METODO FECHAHORA 2
	    public static String fechaHora2()
	    {
	    	//TOMAMOS FECHA DEL SISTEMA
	    	LocalDateTime fechaSistema=LocalDateTime.now();
	    	//DEFINIR FORMATO FECHA 
	    	DateTimeFormatter fecha=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	    	//DAR FORMATO A LA FECHA DEL SISTEMA 
	    	String formatFecha =fecha.format(fechaSistema);
	    	return formatFecha;
	    	
	    }
	    
	    public void eliminarArchivo(String rutaImagen)
	    {
	    	File fichero =new File(rutaImagen);
	    	fichero.delete();
	    }
	    
	    public List<WebElement> listaElementos(By locator, File rutaCarpeta) throws Exception
	    {
	      
	    	List<WebElement> elemento=driver.findElements(locator);
	    	return elemento;
	        
	    }
	    
	    
	    

      //METODO CLIK
	    public void click(By locator, File rutaCarpeta) throws Exception
	    {
	        driver.findElement(locator).click();
	        tiempoEspera(2000);
	        captureScreen(rutaCarpeta, locator, "1");
	    }

     //METODO BORRAR
	    public void clear(By locator, File rutaCarpeta) throws Exception
	    {
	        driver.findElement(locator).clear();
	        captureScreen(rutaCarpeta, locator, "1");
	        tiempoEspera(2000);
	    }



	   //METODO ENVIAR TEXTO
	    public void sendKey(By locator,String inputText,File rutaCarpeta) throws Exception
	    {
	        driver.findElement(locator).sendKeys(inputText);
	        captureScreen(rutaCarpeta, locator, inputText);
	        tiempoEspera(2000);
	    }
	        
	    
	    //METODO ENTER SUBMIN
	    public void submit(By locator, File rutaCarpeta) throws Exception
	    {
	        driver.findElement(locator).submit();
	        captureScreen(rutaCarpeta, locator, "1" );
	        tiempoEspera(2000);
	    }
	   
	    
	    //METODO TIEMPO DE ESPERA
	    public void tiempoEspera(long tiempo) throws InterruptedException
	    {
	        Thread.sleep(tiempo);
	    }
	    
	    public static String fechahora()
	    {
	    	//TOMAMOS FECHA DEL SISTEMA
	    	LocalDateTime fechaSistema = LocalDateTime.now();
	    	//DEFINIR FORMATO FECHA 
	    	DateTimeFormatter fecha = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
	    	//DAR FORMATO A LA FECHA DEL SISTEMA 
	    	String formatFecha =fecha.format(fechaSistema);
	    	return formatFecha;
	    	
	    }
	    
	    public String HoraSistema()
	    {
	    	//TOMAMOS LA FECHA DEL SISTEMA 
	    	LocalTime horaSistema = LocalTime.now();
	    	//DEFINIR FORMATO FECHA 
	    	DateTimeFormatter fecha = DateTimeFormatter.ofPattern("HHmmss");
	    	//DARFORMATO A LA FECHA DEL SISTEMA 
	    	String hora = fecha.format(horaSistema);
	    	return hora;
	    	
	    }
	    
	    public void captureScreen(File rutaCarpeta,By locator,String inputText)throws Exception
        {
            
            String hora = HoraSistema();
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File(rutaCarpeta+"\\"+hora+".png"));
            tiempoEspera(1000);
            String rutaImagen = new File(rutaCarpeta+"\\"+hora+".png").toString();
            
            //INSTANCIAMOS LA CLASE GENERAR PDF
            GenerarPDF informePdf = new GenerarPDF();
            //SE PROCEDE A INSERTAR LOCALIZADOR EN LA IMAGEN PDF
            informePdf.crearBody(locator, rutaImagen);
            
            //ELIMINAR IMAGEN CREADA
            eliminarArchivo(rutaImagen);
            }
        public void captureScreenError(File rutaCarpeta, By locator, String generarEvidencia, String msnError) throws Exception
        {
            if (generarEvidencia.equals("Si"))
            {
                String hora = HoraSistema();
                File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(scrFile, new File(rutaCarpeta+"\\"+hora+".png"));
                String rutaImagen =new File(rutaCarpeta+"\\"+hora+".png").toString();
                
                //INSTACIAMOS LA CLASE GENERAR PDF
                GenerarPDF informePdf = new GenerarPDF();
                //SE PROCEDE A INSERTAR LOCALIZADOR HE IMAGEN EN EL PDF
                informePdf.crearbodyError(locator,rutaImagen,msnError);
                //ELIMINAR IMAGEN CREADA
                
                eliminarArchivo(rutaImagen);
            }
        }
	    
    
	    
	    public File crearCarpeta(String nomTest)
	    {
	    	//ALMACENAMOS LA FECHA EN EL SISTEMA 
	    	String fecha =fechahora();
	    	//CREAMOS EL NOMBRE DE LA CARPETA 
	    	String nomCarpeta =nomTest+"_"+fecha;
	    	//OBTENEMOS LA RUTA DE ALOJAMIENTO DE SALIDA Y EL NOMBRE DEL TEST A EJECUTAR
	    	File directorio =new File("./output/" + nomCarpeta);
	    	//directorio.mkdir();
	    	return directorio; 
	    	
	    }
	}

