package MapsObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import CaseBase.ClaseBase;

public class MapObjectCrearCuenta extends ClaseBase
{
	

		public MapObjectCrearCuenta(WebDriver driver)
		{
		super(driver);
		// TODO Auto-generated constructor stub
		}
	protected By cookies =By.xpath("//div[@class=\"cookie-consent-banner-opt-out\"]/div/button[text()='Entendido']");
	protected By cancelarUbicacion =By.xpath("//button[@class='andes-tooltip-button-close']");
	protected By btnUniversal =By.xpath("//*[@id='root-app']/div/div[2]/section/ol/li[1]/div/div/div[1]/a/div/div/div/div/div/img");
	protected By btncomprarAhora =By.xpath("//button[@class='andes-button andes-spinner__icon-base andes-button--loud']");
	protected By crearCuenta =By.xpath("(//span[@class=\"andes-button__content\"])[1]");
    protected By checkAutorizo =By.id("terms-and-conds");
    protected By Continuar =By.xpath("//*[@class='andes-button__content']");
    protected By confirmarCorreo =By.xpath("//*[@class='andes-button hub-item__button andes-button--medium andes-button--loud']");
    protected By txtCorreo =By.name("email");
    protected By creatuCuenta = By.xpath("//a[@href='https://www.mercadolibre.com.co/registration?confirmation_url=https%3A%2F%2Fwww.mercadolibre.com.co%2F#nav-header']");
   	
}
