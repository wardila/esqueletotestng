package MapsObjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import CaseBase.ClaseBase;

public class MapsObjectInicio extends ClaseBase
{
	
	//CONSTRUCTOR DE LA CLASE
    public MapsObjectInicio(WebDriver driver)
    {
        super(driver); 
    }
    
    //ELEMENTOS PAGINA INICIAL     
    protected By txtBusqueda =By.xpath("//*[@class='nav-search-input']");
    protected By creatuCuenta = By.xpath("//a[@href='https://www.mercadolibre.com.co/registration?confirmation_url=https%3A%2F%2Fwww.mercadolibre.com.co%2F#nav-header']");
    
	
}